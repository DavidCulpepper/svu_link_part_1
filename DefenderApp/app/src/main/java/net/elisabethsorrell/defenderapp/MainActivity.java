package net.elisabethsorrell.defenderapp;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static ArrayList<Weapon> observedWeapons;
    private static ArrayList<Weapon> involvedWeapons;
    private InvolvedAdapter iAdapter;
    private ObservedAdapter oAdapter;
    private RecyclerView involvedWeaponView;
    private RecyclerView observedWeaponView;

    private MyEditTextDatePicker dateOfOffense;
    private MyEditTextDatePicker dateOfArrest;
    private EditText crimeOrOffense;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        dateOfOffense = new MyEditTextDatePicker(this, R.id.doo_Text);
        dateOfArrest = new MyEditTextDatePicker(this, R.id.doa_Text);
        crimeOrOffense = (EditText)findViewById(R.id.coo_Text);

        involvedWeaponView = (RecyclerView) findViewById(R.id.recycler_list_involved);
        involvedWeaponView.setLayoutManager(new LinearLayoutManager(this));
        iAdapter = new InvolvedAdapter(this);
        involvedWeaponView.setAdapter(iAdapter);



        observedWeaponView = (RecyclerView) findViewById(R.id.recycler_list_observed);
        observedWeaponView.setLayoutManager(new LinearLayoutManager(this));
        oAdapter = new ObservedAdapter(this);
        observedWeaponView.setAdapter(oAdapter);

        findViewById(R.id.edit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.plus_involved).setVisibility(View.VISIBLE);
                findViewById(R.id.plus_observed).setVisibility(View.VISIBLE);
            }
        });


    }

    public void initializeWeapons() {
        involvedWeapons = new ArrayList<>();
        for(int i = 0; i < 5; i++){
            involvedWeapons.add(new Weapon("Gun"));
        }


        observedWeapons = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            observedWeapons.add(new Weapon("Knife"));
        }

    }

    /**
     * Class Adapter
     * - The class that is being used to recycle views.
     */
    public static class InvolvedAdapter extends RecyclerView.Adapter<InvolvedViewHolder> {

        public final Activity context;


        /**
         * Constructor that sets up a list of memes to show in a recycled list on
         * an activity called context
         *
         * @param context The activity to show the list on
         */
        public InvolvedAdapter(Activity context) {
            this.context = context;
            involvedWeapons = new ArrayList<>();
            for(int i = 0; i < 5; i++){
                involvedWeapons.add(new Weapon("Gun"));
            }

        }


        @Override
        public InvolvedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


            // --- Find the view that we need
            final View view = context.getLayoutInflater().inflate(R.layout.list_involved_weapons, parent, false);
            return new InvolvedViewHolder(view);
        }

        @Override
        public void onBindViewHolder(InvolvedViewHolder holder, int position) {

            Weapon weapon = involvedWeapons.get(position);

            final InvolvedViewHolder v = holder;                // Accessed by an inner class

            // --- Sets the fields of the regular views and the zoomed views
            holder.weaponText.setText(weapon.type);

        }

        @Override
        public int getItemCount() {
            return involvedWeapons.size();
        }
    }

    /**
     * Class ViewHolder
     * -- Holds all views to be recyled
     */
    public static class InvolvedViewHolder extends RecyclerView.ViewHolder {

        public final TextView weaponText;

        /**
         * Constructor that sets all fields and onClick listeners
         *
         * @param itemView The view that is going to be used to hold everything
         */
        public InvolvedViewHolder(final View itemView) {
            super(itemView);

            weaponText = (TextView) itemView.findViewById(R.id.read_involved_weapon);
        }
    }

    /**
     * Class Adapter
     * - The class that is being used to recycle views.
     */
    public static class ObservedAdapter extends RecyclerView.Adapter<ObservedViewHolder> {

        public final Activity context;


        /**
         * Constructor that sets up a list of memes to show in a recycled list on
         * an activity called context
         *
         * @param context The activity to show the list on
         */
        public ObservedAdapter(Activity context) {
            this.context = context;
            observedWeapons = new ArrayList<>();

            for (int i = 0; i < 5; i++) {
                observedWeapons.add(new Weapon("Knife"));
            }

        }


        @Override
        public ObservedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {



            // --- Find the view that we need
            final View view = context.getLayoutInflater().inflate(R.layout.list_observed_weapons, parent, false);
            return new ObservedViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ObservedViewHolder holder, int position) {

            Weapon weapon = observedWeapons.get(position);



            final ObservedViewHolder v = holder;                // Accessed by an inner class

            // --- Sets the fields of the regular views and the zoomed views
            holder.weaponType.setText(weapon.type);

        }

        @Override
        public int getItemCount() {
            return observedWeapons.size();
        }
    }

    /**
     * Class ViewHolder
     * -- Holds all views to be recyled
     */
    public static class ObservedViewHolder extends RecyclerView.ViewHolder {

        public TextView weaponType;

        /**
         * Constructor that sets all fields and onClick listeners
         *
         * @param itemView The view that is going to be used to hold everything
         */
        public ObservedViewHolder(final View itemView) {
            super(itemView);

            weaponType = (TextView) itemView.findViewById(R.id.read_observed_weapon);
/*
            itemView.findViewById(R.id.plus_observed).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemView.findViewById(R.id.layout_add_observe).setVisibility(View.VISIBLE);
                    itemView.findViewById(R.id.observed_check_mark).setVisibility(View.VISIBLE);
                }
            });*/

        }
    }

    public static class Weapon {
        private String type;

        public Weapon(String type) {
           this.type = type;
        }
        public String getType() {
            return type;
        }
        public void setType(String type) {
            this.type = type;
        }
    }
}
